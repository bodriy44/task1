package com.example.myapplication.view;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import android.widget.EditText;

import com.example.myapplication.R;
import com.example.myapplication.model.MainModel;
import com.example.myapplication.presenter.MainPresenter;

public class MainActivity extends AppCompatActivity implements ViewInterface{

    private MainPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    private void init(){
        findViewById(R.id.button).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                presenter.addNote();
            }

        });
        presenter = new MainPresenter(this);
    }

    @Override
    public void showSaveNotify(String notify) {
        Toast toast = Toast.makeText(this, notify, Toast.LENGTH_LONG);
        toast.show();
    }

    @Override
    public String getData()
    {
        return ((EditText)this.findViewById(R.id.editTextTextPersonName)).getText().toString();
    }
}

