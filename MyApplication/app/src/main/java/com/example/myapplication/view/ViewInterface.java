package com.example.myapplication.view;

public interface ViewInterface {

    String getData();
    void showSaveNotify(String notify);
}
